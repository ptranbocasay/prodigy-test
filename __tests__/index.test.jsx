import { render, screen } from "@testing-library/react";
import Home from "../pages/index";
import { products } from "../data";

describe("Home", () => {
  const container = render(<Home products={products} />);
  it("should render home heading", () => {
    const heading = screen.getByRole("heading", {
      name: /best insurance solution/i,
    });
    expect(heading).toBeInTheDocument();
  });
});
