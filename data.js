export const products = [
  {
    id: "1",
    title: "Portable Term Life Insurance",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "The red line moved across the page. With each millimeter it advanced forward, something changed in the room. The actual change taking place was difficult to perceive, but the change was real. The red line continued relentlessly across the page and the room would never be the same. It was always the Monday mornings. It never seemed to happen on Tuesday morning, Wednesday morning, or any other morning during the week. But it happened every Monday morning like clockwork. He mentally prepared himself to once again deal with what was about to happen, but this time he also placed a knife in his pocket just in case.",
  },
  {
    id: "2",
    title: "Cancer Insurance",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "Funds the fight for one of the most costly-to-treat illnesses",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "GitHub today announced a bunch of new features at its virtual GitHub Universe conference including dark mode, auto-merge pull requests, and Enterprise Server 3.0. In the past couple of years, almost all major apps have rolled out a dark theme for its users, so why not GitHub?",
  },
  {
    id: "3",
    title: "Everyday Solutions",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "The red line moved across the page. With each millimeter it advanced forward, something changed in the room. The actual change taking place was difficult to perceive, but the change was real. The red line continued relentlessly across the page and the room would never be the same. It was always the Monday mornings. It never seemed to happen on Tuesday morning, Wednesday morning, or any other morning during the week. But it happened every Monday morning like clockwork. He mentally prepared himself to once again deal with what was about to happen, but this time he also placed a knife in his pocket just in case.",
  },
  {
    id: "4",
    title: "Pet Insurance",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "The red line moved across the page. With each millimeter it advanced forward, something changed in the room. The actual change taking place was difficult to perceive, but the change was real. The red line continued relentlessly across the page and the room would never be the same. It was always the Monday mornings. It never seemed to happen on Tuesday morning, Wednesday morning, or any other morning during the week. But it happened every Monday morning like clockwork. He mentally prepared himself to once again deal with what was about to happen, but this time he also placed a knife in his pocket just in case.",
  },
  {
    id: "5",
    title: "Mental Health Platform",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "The red line moved across the page. With each millimeter it advanced forward, something changed in the room. The actual change taking place was difficult to perceive, but the change was real. The red line continued relentlessly across the page and the room would never be the same. It was always the Monday mornings. It never seemed to happen on Tuesday morning, Wednesday morning, or any other morning during the week. But it happened every Monday morning like clockwork. He mentally prepared himself to once again deal with what was about to happen, but this time he also placed a knife in his pocket just in case.",
  },
  {
    id: "6",
    title: "Mental Health Platform",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "GitHub today announced a bunch of new features at its virtual GitHub Universe conference including dark mode, auto-merge pull requests, and Enterprise Server 3.0. In the past couple of years, almost all major apps have rolled out a dark theme for its users, so why not GitHub?",
  },
  {
    id: "7",
    title: "Identity Theft Protection",
    overview:
      "Founded in 1945, American Public Life Insurance Company (APL) is a leading provider of voluntary worksite benefits. With sales through independent brokers, APL is changing employee benefits by providing a fully-customizable suite of products to meet the specific needs of groups. With offices in Jackson, Mississippi and Oklahoma City, Oklahoma, APL is licensed to conduct business in 49 states and has a financial rating of A+ (Superior)* through AM Best (www.ambest.com). For more information, please visit www.ampublic.com.",
    hih: [
      "May offer all three terms - 10, 20 and 30-year.",
      "Group rate.",
      "Level rates throughout the life of the term.",
    ],
    keys: ["Underwritten", "Group rate.", "Benefits "],
    excerpt:
      "GitHub today announced a bunch of new features at its virtual GitHub...",
    body: "GitHub today announced a bunch of new features at its virtual GitHub Universe conference including dark mode, auto-merge pull requests, and Enterprise Server 3.0. In the past couple of years, almost all major apps have rolled out a dark theme for its users, so why not GitHub?",
  },
];
