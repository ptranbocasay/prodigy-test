This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## App feature
- 3 page: Home, About, Contact
- Insurance product list view
- Insurance product detail page
- Search for product
- Add/Remove product to favourite list
- Responsive

## Project Detail
- page:  included all page and api route
- public: static content
- styles: page style and component style
- config: project config for base URL, env variable, ..
- component: all component use in project
- atoms: recoiljs state atom
- __test__: project unit test
- data.js: mock data