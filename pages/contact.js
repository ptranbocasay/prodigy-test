import Meta from "../components/Meta";
import contactStyles from "../styles/Contact.module.css";

const contact = () => {
  return (
    <div className="base-container">
      <Meta title="Contact" />
      <h1>Contact</h1>
      <p>
        This is important to remember. Love isn&apos;t like pie. You don&apos;t
        need to divide it among all your friends and loved ones. No matter how
        much love you give, you can always give more. It doesn&apos;t run out,
        so don&apos;t try to hold back giving it as if it may one day run out.
        Give it freely and as much as you want.
      </p>

      <h2 className={contactStyles.heading}>Our office</h2>
      <div className={contactStyles.offices}>
        <div className={contactStyles.office}>
          <h4>Indonesia</h4>
          <div className={contactStyles.officeDetail}>
            <ul>
              <li>657 Saint Paul, Example City, Magic Country</li>
              <li>Phone: +65 12312313</li>
              <li>Fax: +65 12312313</li>
              <li>Email: prodigy@prodigy.com</li>
            </ul>
          </div>
        </div>
        <div className={contactStyles.office}>
          <h4>Singapore</h4>
          <div className={contactStyles.officeDetail}>
            <ul>
              <li>657 Saint Paul, Example City, Magic Country</li>
              <li>Phone: +65 12312313</li>
              <li>Fax: +65 12312313</li>
              <li>Email: prodigy@prodigy.com</li>
            </ul>
          </div>
        </div>
        <div className={contactStyles.office}>
          <h4>Thailand</h4>
          <div className={contactStyles.officeDetail}>
            <ul>
              <li>657 Saint Paul, Example City, Magic Country</li>
              <li>Phone: +65 12312313</li>
              <li>Fax: +65 12312313</li>
              <li>Email: prodigy@prodigy.com</li>
            </ul>
          </div>
        </div>
        <div className={contactStyles.office}>
          <h4>Philippin</h4>
          <div className={contactStyles.officeDetail}>
            <ul>
              <li>657 Saint Paul, Example City, Magic Country</li>
              <li>Phone: +65 12312313</li>
              <li>Fax: +65 12312313</li>
              <li>Email: prodigy@prodigy.com</li>
            </ul>
          </div>
        </div>
        <div className={contactStyles.office}>
          <h4>Japan</h4>
          <div className={contactStyles.officeDetail}>
            <ul>
              <li>657 Saint Paul, Example City, Magic Country</li>
              <li>Phone: +65 12312313</li>
              <li>Fax: +65 12312313</li>
              <li>Email: prodigy@prodigy.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default contact;
