import { server } from "../../../config";
import Link from "next/link";
import Meta from "../../../components/Meta";
import detailStyles from "../../../styles/ProductDetail.module.css";

const product = ({ product }) => {
    
  return (
    <>
      <Meta title={product.title} description={product.excerpt} />
      <div className="base-container">
        <Link href="/">
          <a className={detailStyles.back}>Go Back</a>
        </Link>
        <h1>{product.title}</h1>
        <h2>Overview</h2>
        <p>{product.overview}</p>

        <h2>How it helps</h2>
        <ul>
          {product.hih.map((h, idx) => (
            <li key={idx}>{h}</li>
          ))}
        </ul>
        <h2>Available Products</h2>
        <ul className={detailStyles.tag}>
          {product.keys.map((k, idx) => (
            <li key={idx}>{k}</li>
          ))}
        </ul>
        <h2>Detail</h2>
        <p>{product.body}</p>
      </div>
      <br />
    </>
  );
};

export const getStaticProps = async (context) => {
  const res = await fetch(`${server}/api/products/${context.params.id}`);

  const product = await res.json();

  return {
    props: {
      product,
    },
  };
};

export const getStaticPaths = async () => {
  const res = await fetch(`${server}/api/products`);

  const products = await res.json();

  const ids = products.map((product) => product.id);
  const paths = ids.map((id) => ({ params: { id: id.toString() } }));

  return {
    paths,
    fallback: false,
  };
};

export default product;
