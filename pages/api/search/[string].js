import { products } from "../../../data";

export default function handler({ query: { string } }, res) {
  const filtered = products.filter((product) => product.title.toLowerCase().includes(string));
  if (filtered.length > 0) {
    res.status(200).json(filtered);
  } else {
    res
      .status(404)
      .json({ message: `There is no product match "${string}"` });
  }
}
