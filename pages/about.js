import Meta from "../components/Meta";

const about = () => {
  return (
    <div className="base-container">
      <Meta title="About" />
      <h1>About</h1>
      <p>
        This is important to remember. Love isn&apos;t like pie. You don&apos;t need to
        divide it among all your friends and loved ones. No matter how much love
        you give, you can always give more. It doesn&apos;t run out, so don&apos;t try to
        hold back giving it as if it may one day run out. Give it freely and as
        much as you want.
      </p>
      <p>
        If you&apos;re looking for random paragraphs, you&apos;ve come to the right place.
        When a random word or a random sentence isn&apos;t quite enough, the next
        logical step is to find a random paragraph. We created the Random
        Paragraph Generator with you in mind. The process is quite simple.
        Choose the number of random paragraphs you&apos;d like to see and click the
        button. Your chosen number of paragraphs will instantly appear. While it
        may not be obvious to everyone, there are a number of reasons creating
        random paragraphs can be useful. A few examples of how some people use
        this generator are listed in the following paragraphs.
      </p>
      <h2>Creative Writing</h2>
      <p>
        Generating random paragraphs can be an excellent way for writers to get
        their creative flow going at the beginning of the day. The writer has no
        idea what topic the random paragraph will be about when it appears. This
        forces the writer to use creativity to complete one of three common
        writing challenges. The writer can use the paragraph as the first one of
        a short story and build upon it. A second option is to use the random
        paragraph somewhere in a short story they create. The third option is to
        have the random paragraph be the ending paragraph in a short story. No
        matter which of these challenges is undertaken, the writer is forced to
        use creativity to incorporate the paragraph into their writing.
      </p>
      <h2>Tackle Writers&apos; Block</h2>
      <p>
        A random paragraph can also be an excellent way for a writer to tackle
        writers&apos; block. Writing block can often happen due to being stuck with a
        current project that the writer is trying to complete. By inserting a
        completely random paragraph from which to begin, it can take down some
        of the issues that may have been causing the writers&apos; block in the first
        place.
      </p>
      <h2>Beginning Writing Routine</h2>
      <p>
        Another productive way to use this tool to begin a daily writing
        routine. One way is to generate a random paragraph with the intention to
        try to rewrite it while still keeping the original meaning. The purpose
        here is to just get the writing started so that when the writer goes
        onto their day&apos;s writing projects, words are already flowing from their
        fingers.
      </p>

      <h2>Writing Challenge</h2>
      <p>
        Another writing challenge can be to take the individual sentences in the
        random paragraph and incorporate a single sentence from that into a new
        paragraph to create a short story. Unlike the random sentence generator,
        the sentences from the random paragraph will have some connection to one
        another so it will be a bit different. You also won&apos;t know exactly how
        many sentences will appear in the random paragraph.
      </p>
    </div>
  );
};

export default about;
