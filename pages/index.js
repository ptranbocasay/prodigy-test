import { server } from "../config";
import ProductList from "../components/ProductList";
import Header from "../components/Header";
import Search from "../components/Search";
import { useState } from "react";
import FavouriteItem from "../components/FavouriteItem";
import { useRecoilState } from "recoil";
import { favItemState } from "../atoms/FavItemAtom";
import Notification from "../components/Notification";

export default function Home({ products }) {
  const [currentProducts, setCurrentProducts] = useState(products);
  const [favProducts, setFavProducts] = useRecoilState(favItemState);
  const [notification, setNotification] = useState({});

  const onSearchChange = async (string) => {
    const res = await fetch(`${server}/api/search/${string}`);
    const products = await res.json();
    setCurrentProducts(products);
  };

  const addToFavourite = (v) => {
    const selectedProduct = products.filter((p) => p.id === v);
    if (favProducts.filter((p) => p.id === v).length > 0) {
      return;
    } else {
      setFavProducts((curr) => [...curr, ...selectedProduct]);
      setNotification({ product: selectedProduct[0], action: "ADD" });
    }
  };

  const onRemoveItem = (id) => {
    setFavProducts((curr) => curr.filter((p) => p.id !== id));
    const selectedProduct = products.filter((p) => p.id === id);
    setNotification({ product: selectedProduct[0], action: "REMOVE" });
  };

  return (
    <div className="base-container">
      <Header />
      <Search onSearchChange={onSearchChange} />
      {!currentProducts.message ? (
        <ProductList
          products={currentProducts}
          addToFavourite={addToFavourite}
        />
      ) : (
        <p className="text-center">{currentProducts.message}</p>
      )}
      <FavouriteItem products={favProducts} onRemoveItem={onRemoveItem} />
      {notification.product ? <Notification notification={notification} /> : ""}
    </div>
  );
}

export const getStaticProps = async () => {
  const res = await fetch(`${server}/api/products`);
  const products = await res.json();

  return {
    props: {
      products,
    },
  };
};
