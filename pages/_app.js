import Layout from '../components/Layout'
import '../styles/globals.css'
import { RecoilRoot } from "recoil";

function ProdigyApp({ Component, pageProps }) {
  return (
    <RecoilRoot>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </RecoilRoot>
  )
}

export default ProdigyApp
