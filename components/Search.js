import searchStyles from "../styles/Search.module.css";

const Search = ({ onSearchChange }) => {
  const onInputChange = (e) => {
    const { value } = e.target;
    onSearchChange(value);
  };
  return (
    <div className={searchStyles.wrap}>
      <h3 className="text-center">Find your product</h3>
      <div className={searchStyles.inputWrap}>
        <input
          className={searchStyles.input}
          type="text"
          placeholder="Enter name, keywords"
          onChange={onInputChange}
        />
      </div>
    </div>
  );
};
export default Search;
