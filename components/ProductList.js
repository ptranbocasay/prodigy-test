import ProductItem from './ProductItem'
import productStyles from '../styles/Product.module.css'
import FavButton from './button/FavButton'

const ProductList = ({ addToFavourite,  products }) => {
  const addFav = (v) => {
    addToFavourite(v);;
  };
  
  return (
    <div className={productStyles.grid}>
      {products.map((product, idx) => (
        <div className={productStyles.cardWrap} key={idx}>
          <ProductItem product={product} />
          <FavButton addFav={addFav} productId={product.id} />
        </div>
      ))}
    </div>
  )
}

export default ProductList
