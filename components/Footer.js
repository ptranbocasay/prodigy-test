import Image from "next/image";
import Link from "next/link";
import footerStyles from "../styles/Footer.module.css";

const Footer = () => {
  return (
    <div className={footerStyles.wrapper}>
      <div className="base-container">
        <div className="text-center">
          <Image
            src="/footer.png"
            alt="Photo"
            width={80}
            height={80}
            priority
            className="next-image"
          />
        </div>
        <div className={footerStyles.grid}>
          <div className={footerStyles.information}>
            <h2 className={footerStyles.title}>Best Insurance solution</h2>
            <p className={footerStyles.description}>
              While something that is not pleasant to think about, the possibility of
              leaving behind debts, current and future expenses can dramatically
              impact loved ones and their lifestyle.
            </p>
          </div>
          <div className={footerStyles.contact}>
            <ul className={footerStyles.social}>
              <li>
                <Link href='/'>facebook</Link>
              </li>
              <li>
                <Link href='/'>twitter</Link>
              </li>
              <li>
                <Link href='/'>Youtube</Link>
              </li>
              <li>
                <Link href='/'>Instagram</Link>
              </li>
            </ul>
          </div>
        </div>
        <div className={`text-center ${footerStyles.copyright}`}>
          Make with love by Luke Tran
        </div>
      </div>
    </div>
  );
};

export default Footer;
