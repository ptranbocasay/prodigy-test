import ProductItem from "./ProductItem";
import favouriteItemStyles from "../styles/FavouriteItem.module.css";
import Image from "next/image";
import { useState } from "react";
import RemoveButton from "./button/RemoveButton";

const FavouriteItem = ({ onRemoveItem, products }) => {
  const [show, setShow] = useState(false);
  const toggleFavourite = () => {
    setShow(!show);
  };

  const onRemove = (id) => {
    onRemoveItem(id);
  };

  return (
    <div className={`${favouriteItemStyles.wrap} ${show ? "show" : null}`}>
      <div className={favouriteItemStyles.icon} onClick={toggleFavourite}>
        {!show ? (
          <Image
            src="/eyeglass.svg"
            alt="Photo"
            width={30}
            height={30}
            priority
          />
        ) : (
          <Image src="/close.svg" alt="Photo" width={30} height={30} priority />
        )}
      </div>
      <div className={favouriteItemStyles.productWrap}>
        {products.length > 0 ? (
          products.map((product, idx) => (
            <div className={favouriteItemStyles.cardWrap} key={idx}>
              <ProductItem product={product} />
              <RemoveButton onRemove={onRemove} productId={product.id} />
            </div>
          ))
        ) : (
          <p>Favourite list is empty</p>
        )}
      </div>
    </div>
  );
};

export default FavouriteItem;
