import Link from 'next/link'
import Image from 'next/image'
import navStyles from '../styles/Nav.module.css'

const Nav = () => {
  return (
    <div className={navStyles.wrap}>
      <div className="base-container">
        <nav className={navStyles.nav}>
          <Link href='/'>
            <a>
              <Image
                src="/prodigy.svg"
                alt="Photo"
                width={176}
                height={67}
                priority
                className="next-image"
              />
            </a>
          </Link>
          <ul>
            <li>
              <Link href='/'>Home</Link>
            </li>
            <li>
              <Link href='/about'>About</Link>
            </li>
            <li>
              <Link href='/contact'>Contact</Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Nav
