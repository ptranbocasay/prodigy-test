import Link from 'next/link'
import productStyles from '../styles/Product.module.css'

const ProductItem = ({ product }) => {
  return (
    <Link href={`/product/${product.id}`}>
      <a className={productStyles.card} data-testid={`product_${product.id}`}>
        <h3>{product.title} &rarr;</h3>
        <p>{product.excerpt}</p>
      </a>
    </Link>
  )
}

export default ProductItem
