import removeButtonStyles from '../../styles/RemoveButton.module.css'

const RemoveButton = ({ onRemove, productId }) => {
  const removeItem = () => {
    onRemove(productId);
  };
  
  return (
    <div className={removeButtonStyles.wrap}>
      <button onClick={removeItem}>Delete</button>
    </div>
  )
}

export default RemoveButton
