import favButtonStyles from '../../styles/FavButton.module.css'

const FavButton = ({ addFav, productId }) => {
  const addFavourite = () => {
    addFav(productId);
  };
  
  return (
    <div className={favButtonStyles.wrap}>
      <button onClick={addFavourite}>Add to Favourite</button>
    </div>
  )
}

export default FavButton
