import { useEffect, useState } from 'react';
import notificationStyles from '../styles/Notification.module.css'

const Notification = ({ notification }) => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    setShow(true)
    setTimeout(() => setShow(false), 50);
  },[notification])
  return (
    <div className={`${notificationStyles.wrap} ${!show ? notificationStyles.hide : ''}`}>
      {notification.product.title} {notification.action === "ADD" ? 'added to' : 'remove from'} favourite list !
    </div>
  )
}

export default Notification
