import headerStyles from '../styles/Header.module.css'

const Header = () => {
  return (
    <div className='base-container'>
      <h1 className={headerStyles.title}>
        Best Insurance solution
      </h1>
      <p className={headerStyles.description}>
      While something that is not pleasant to think about, the possibility of leaving behind debts, current and future expenses can dramatically impact loved ones and their lifestyle. In the event of the unexpected, help employees make sure that those nearest and dearest to them are impacted less financially. From term to permanent life insurance options such as universal, variable universal life and whole life policies, the full spectrum of life insurance solutions are available.
      </p>
    </div>
  )
}

export default Header
